
salud = 0.04
pension = 0.12
empleados = [["NOMBRE","SALARIO","SALUD","PENSION","SALDO"]]
while True:
    
    print("Nómina empresarial")
    print("1. Administrar nómina")
    print("2. Ver reporte")

    opcion = input()
    
    if opcion == "1": 
        print("Creación de empleado")

        nombre = input("Nombre empleado: ")
        salario = int(input("Salario empleado: $"))
        dias = input("Días laborados empleado: #")

        s = int(salario*salud)
        p = int(salario*pension)
        saldo = salario - (s+p)

        empleados.append([nombre, salario,s,p,saldo])
    elif opcion == "2":
        print("Reporte:")
        i = 0
        saldos = 0
        while i < len(empleados):
            if i>0:
                saldos += empleados[i][4]
            print(empleados[i][0], " | ",empleados[i][1], " | ",empleados[i][2], " | ",empleados[i][3], " | ",empleados[i][4])
            i += 1

        print("PROMEDIO SALDO: $",int(saldos / (len(empleados)-1)))